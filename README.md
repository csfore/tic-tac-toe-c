# Tic Tac Toe

If you don't know what Tic-Tac-Toe is, DM me for assistance

## Building

### Using Makefile

- `make run`

### Manual

- I used Clang but GCC probably works
- `clang main.c`
- `./a.out`

## TODO

- [ ] Add a win state
- [ ] Make it readable