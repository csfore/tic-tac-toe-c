#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>

#define a_val 97
#define b_val 98
#define c_val 99

int check_win(char* board[4][3]) {
    printf("====BOARD CHECK====\n");

    char* checked[3];

    // Checking column
    for (int i=1; i < 4; i++) {
        printf("%s", *board[i]);
        printf("%d", i);
        checked[i-1] = *board[i];
    }
    printf("\n===Checked===\n%s\n===Checked===\n", *checked);
    printf("\n====BOARD CHECK====");

    return 1;
}

int main() {
    // char* shadowb = "#########";
    int shadowb = 0b000000000;

    bool win_state = false;

    char move[2];

    int get_int;

    int player = 0;

    int turn_count = 0;

    char* arr[4][3] = {
        {"a", "b", "c"}, 
        {"[ ]", "[ ]", "[ ]"}, 
        {"[ ]", "[ ]", "[ ]"}, 
        {"[ ]", "[ ]", "[ ]"}
    };

    while (win_state != true) {
        printf("\n");
        printf("Turn: %d\n", turn_count);
        for (int i=1; i < 4; i++) {
            // printf("%d", i);
            printf("%d ", i);
            for (int j=0; j < 3; j++) {
                printf("%s", arr[i][j]);
            }
            printf("\n");
        }
        printf("   A  B  C\n");

        printf("\nYour Move: ");
        scanf("%s", move);

        get_int = (tolower(move[0]) - a_val);

        int column_selected = strtol(&move[1], (char **)NULL, 10);

        if (turn_count % 2 == 0) {
            arr[column_selected][get_int] = "[X]";
            // shadowb[column_selected + (get_int + 1)] = 1;
        } else {
            arr[column_selected][get_int] = "[O]";
            // shadowb[column_selected + (get_int + 1)] = 1;
        }
        printf("%d\n", (column_selected * (get_int)));
        turn_count++;
        check_win(arr);
    }

    return 0;
}