CC = clang
CFLAGS = -Wall -Wextra -Wshadow -O

run:
	$(CC) $(CFLAGS) -o tic-tac-toe main.c
	./tic-tac-toe